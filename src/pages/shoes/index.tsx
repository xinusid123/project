import React from 'react';
import { Navbar } from 'src/components';

const Shoes: React.FC = () => {
  return (
    <div>
      <Navbar />
      <div className="relative bg-gradient-to-tr to-[#00000069] from-[#00000085]">
        <img
          src="https://cdn.shopify.com/s/files/1/1811/9799/files/breadcrumb-4.jpg?v=1635497688"
          alt="logo"
          className="w-full h-[50vh] object-cover mix-blend-overlay"
        />
        <h5 className="absolute inset-y-1/2 left-[40%] text-white text-5xl tracking-wider leading-9">
          Product
        </h5>
      </div>
    </div>
  );
};

export default Shoes;
