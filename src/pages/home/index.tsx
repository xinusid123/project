import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Navbar } from 'src/components';
import Card from './components/Card';

interface IProps {}

const App: React.FC<IProps> = ({}) => {
  return (
    <div>
      <Navbar />
      <div className="relative">
        <img
          src="https://cdn.shopify.com/s/files/1/1811/9799/files/shoe7.jpg?v=1494405629"
          alt="banner"
          className="object-cover h-[85vh] w-full"
        />
        <div className="absolute bottom-[35%] left-[10%]">
          <h1 className="text-white text-5xl"> Welcome to the Shoe Zone</h1>
          <h3 className=" bg-black inline-block p-3 cursor-pointer text-white rounded mt-5">
            Book Now
          </h3>
        </div>
      </div>
      <Container>
        <h5 className="text-center text-4xl">Our Collection</h5>

        <div className="grid gap-5 grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 my-20">
          {Array(10)
            .fill('')
            .map((_, i) => (
              <Link key={i} to="/product">
                <Card
                  imageUrl="https://cdn.pixabay.com/photo/2016/10/28/09/41/jordan-shoes-1777572_960_720.jpg"
                  price="2000"
                  heading="Sneaker Shoes"
                  subTitle="This is the best shoes that are found in thisedijfdkjdfjkdfkja dfijhdfjhfd jhdfhjdrhjdfhh"
                />
              </Link>
            ))}
        </div>
      </Container>
    </div>
  );
};

export default React.memo(App);
