import React from 'react';

interface IProps {
  imageUrl: string;
  price: string;
  heading: string;
  subTitle: string;
}

const Card: React.FC<IProps> = ({ imageUrl, price, heading, subTitle }) => {
  return (
    <div className="rounded-md border-2 border-gray-200">
      <img
        src={imageUrl}
        alt="logo"
        className="w-full h-56 object-cover object-center rounded"
      />
      <div className="p-3">
        <div className="flex items-center justify-between my-5">
          <h3 className="text-center text-black text-opacity-75">
            {' '}
            {heading}{' '}
          </h3>
          <p className="text-xs font-bold"> {price} </p>
        </div>
        <p className="text-xs text-black"> {subTitle} </p>
      </div>
    </div>
  );
};

export default Card;
