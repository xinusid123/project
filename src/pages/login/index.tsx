import { Form, Formik } from 'formik';
import React from 'react';
import { TextField } from 'src/components';
import * as Yup from 'yup';
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import { FcGoogle } from 'react-icons/fc';

interface IProps {}

const FORM_VALIDATION = Yup.object().shape({
  email: Yup.string()
    .email()
    .typeError('Please enter a valid email address')
    .required('Email is required'),
  password: Yup.string()
    .min(6, 'Please enter at least 6 character long')
    .required('Password is required'),
});

const Login: React.FC<IProps> = ({}) => {
  const [showPassword, setShowPassword] = React.useState(false);
  const initValues = {
    email: '',
    password: '',
  };

  return (
    <div className="grid gap-5 md:grid-cols-2 grid-cols-1 bg-[#FFFFFF] h-full">
      <div className="p-5 flex-col flex items-center justify-center">
        <div className="mt-10">
          <h4 className="font-medium text-4xl mt-10">Welcome Back</h4>
          <p className="text-xs text-black text-opacity-60 my-2">
            Welcome back. Please enter your details
          </p>
        </div>
        <Formik
          validationSchema={FORM_VALIDATION}
          validateOnMount
          onSubmit={(val) => console.log('', val)}
          initialValues={initValues}
        >
          {({}) => (
            <Form className="mb-3">
              <div className="my-5">
                <TextField
                  name="email"
                  label="Email Address"
                  placeholder="Enter email address"
                />
              </div>
              <div className="my-5 relative">
                {!showPassword ? (
                  <AiFillEye
                    className="absolute right-3 top-9 cursor-pointer"
                    onClick={() => setShowPassword(!showPassword)}
                  />
                ) : (
                  <AiFillEyeInvisible
                    className="absolute right-3 top-9 cursor-pointer"
                    onClick={() => setShowPassword(!showPassword)}
                  />
                )}
                <TextField
                  name="password"
                  label="Password"
                  placeholder="Enter password"
                  type={!showPassword ? 'password' : 'text'}
                />
              </div>
              <div className="pt-4 mb-4">
                <button
                  className="bg-black hover:bg-white hover:ring-1 hover:text-black hover:ring-blue-600 px-3 w-full p-3 rounded-lg text-white text-sm"
                  type="submit"
                >
                  Sign in
                </button>
                <div className="my-5 flex items-center justify-center border-2 cursor-pointer border-gray-100 shadow-sm px-3 w-full p-2 rounded-lg text-black text-sm">
                  <FcGoogle size={25} className="mr-5" /> Sign in with Google
                </div>
              </div>
            </Form>
          )}
        </Formik>
        <p className="text-xs text-black text-opacity-60 my-2">
          Don't have an account?{' '}
          <span className="font-bold text-black ml-1">Sign up for free</span>
        </p>
      </div>
      <img
        src="https://cdn.pixabay.com/photo/2016/11/29/03/30/adult-1867073_960_720.jpg"
        alt="logo"
        className="h-screen w-full object-center object-cover"
      />
    </div>
  );
};

export default React.memo(Login);
