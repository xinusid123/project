import React from 'react';
import { Link } from 'react-router-dom';
import Container from '../container';

const Navbar: React.FC = () => {
  return (
    <Container notPadding>
      <div className="flex items-center justify-between">
        <img
          src="https://cdn.shopify.com/s/files/1/1811/9799/files/shoe-logo-new_300x300.png?v=1494509682"
          alt="logo"
          className="w-48"
        />
        <div className="flex items-center space-x-5">
          <p className="text-sm text-black text-opacity-50">Home</p>
          <p className="text-sm text-black text-opacity-50">About us</p>
          <p className="text-sm text-black text-opacity-50">Contat us</p>
          <p className="text-sm text-black text-opacity-50">Products</p>
          <Link to="/login">
            <button className="bg-black text-white px-4 py-2 rounded text-sm">
              sign in
            </button>
          </Link>
          <button>Sign up</button>
        </div>
      </div>
    </Container>
  );
};

export default Navbar;
// https://cdn.shopify.com/s/files/1/1811/9799/files/breadcrumb-4.jpg?v=1635497688
