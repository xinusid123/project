import React from 'react';
import { ErrorMessage, FieldHookConfig, useField } from 'formik';
interface IProps {
  label: string | number;
  placeholder?: string;
  type?: string;
  defaultValue?: string | number;
  readOnly?: boolean;
  hidden?: boolean;
  ref?: any;
}
const TextField = ({
  label,
  placeholder,
  type,
  readOnly,
  defaultValue,
  hidden,
  ref,
  ...otherProps
}: IProps & FieldHookConfig<string>) => {
  const [field, meta] = useField(otherProps);
  return (
    <div className="mb-1">
      <label
        className={`font-normal text-sm mb-1 text-secondary block ${
          meta.error && meta.touched ? 'text-[#ff0f0b]' : ''
        } `}
        htmlFor={field.name}
      >
        {label}
      </label>
      <input
        className={`shadow w-full appearance-none ring-2 rounded py-2 block px-3 ring-secondary ring-opacity-30 bg-pure-white focus:outline-none focus:shadow-outline placeholder:text-xs"  ${
          meta.touched && meta.error ? 'ring-[#ff0019] ring-3' : ''
        } ${!meta.error ? 'is-valid' : ''}`}
        {...field}
        type={type}
        placeholder={placeholder}
        defaultValue={defaultValue}
        autoComplete="off"
        readOnly={readOnly}
      />
      <ErrorMessage
        component="div"
        name={field.name}
        className="bg-[#e4655f] capitalize text-white mt-1 text-xs inline-block py-1 px-2 mb-3"
      />
    </div>
  );
};

export default React.memo(TextField);
