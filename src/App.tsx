import React from 'react';
import { Route, Router, Routes } from 'react-router-dom';
import { Home, Login, Shoes } from './pages';

interface IProps {}

const App: React.FC<IProps> = ({}) => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/login" element={<Login />} />
      <Route path="/product" element={<Shoes />} />
    </Routes>
  );
};

export default React.memo(App);
